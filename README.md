# TASK MANAGER

## SCREENS

https://drive.google.com/drive/folders/1hlL0blsDiaHj0xboJATWRLzNSwLz9Zg5

## DEVELOPER INFO

name: Dmitry Gavran

e-mail: dgavran@tsconsulting.com

e-mail: support@tsconsulting.com

## SOFTWARE

System: Windows 10

Version JDK: 1.8.0_282

## HARDWARE

CPU: i5

RAM: 16GB

HDD: 250GB

## PRORAM RUN

```bash
java -jar ./task-manager.jar
```
